﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZpNewGame.Pieces
{
    internal class Destroyer
    {
        Guid id;
        bool isGold;
        bool isAlive = true;
        Tuple<int, int> Position;

        Destroyer(int X, int Y, bool gold)
        {
            id = Guid.NewGuid();
            Position = new Tuple<int, int>(X, Y);
            isGold = gold;
        }
    }
}
