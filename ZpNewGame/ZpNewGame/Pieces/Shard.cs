﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZpNewGame.Pieces
{
    internal class Shard
    {
        Guid id { get; set; }
        bool isAlive = true;
        bool isGold { get; set; }
        int MovementSpeed = 1;
        bool isNextToProtector = false;
        Tuple<int, int> Position;
        string[] MovemementDirections = new string[5] { "W", "NW", "N", "NE", "E" };

        Shard(int X, int Y, bool gold)
        {
            Position = new Tuple<int, int>(X, Y);
            isGold = gold;
            id = Guid.NewGuid();
        }
    }
}
