﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZpNewGame.Pieces
{
    internal class Slasher
    {
        Guid id;
        bool isGold;
        bool isAlive = true;
        Tuple<int, int> Position;

        Slasher(int X, int Y, bool gold)
        {
            Position = new Tuple<int, int>(X, Y);
            isGold = gold;
            id = Guid.NewGuid();
        }
    }
}
