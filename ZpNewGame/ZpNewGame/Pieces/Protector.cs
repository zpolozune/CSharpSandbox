﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZpNewGame.Pieces
{
    public class Protector
    {
        Guid id { get; set; }
        bool isGold { get; set; }
        bool isAlive = true;
        int MovementSpeed = 1;
        bool isNextToProtect = true;
        Tuple<int?, int?>? Position;

        Protector(int X, int Y, bool gold)
        {
            Position = new Tuple<int?, int?>(X, Y);
            isGold = gold;
            id = Guid.NewGuid();
        }
    }
}
