﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZpNewGame.Pieces
{
    internal class Queen
    {
        Guid id;
        Tuple<int, int> Position;
        bool isGold;
        bool isAlive = true;

        Queen(int X, int Y, bool gold)
        {
            id = Guid.NewGuid();
            Position = new Tuple<int, int>(X, Y);
            isGold = gold;
        }
    }
}
