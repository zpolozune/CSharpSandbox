﻿# ZpNewGame Overview
## Board
8 x 8, like a chess board

## Pieces
### Shards
- You get 8 of them lining your front row.
- They function like pawns except they can move diagonally whenever they want.
- They can initiate an attack on another piece with a 50%/50% success chance. If they lose, they die.

### Convertors
- You get two of these on the far sides of your back row.
- They can move like bishops except instead of capturing the piece they initiate a conversion. 
- For a conversion, the other piece gets a roll to save and see if it gets converted or not; the chance is 75% convert / 25% failure. When a conversion is initiated it counts as a move.
- A failed conversion disabled the Convertor for one move.

### Slashers
- You get two of these and they are on the back row, one in from the Convertors.
- These move like rooks.
- They can initiate an attack and take a piece out of play by moving to its space. The piece get a roll to save in which case nothing happens. If it fails, the attacked piece is removed from play. The chances are 75% killing / 25% nothing happening. This counts as a move.
- If the Slasher successfully kills a piece it can then move again.
- If the Slasher fails to kill a piece it has finished its turn and survies as mentioned before.


### Queens
- You get two of these on the back row, one in from the Slashers.
- These can move like a queen in chess.
- They can attack a piece if it is within line of sight and kill it. There is no save for the piece; it just dies.
- They must rest for one turn after an attack and cannot be moved again.

### Protector
- You get one of these on the right side of the remaining back row squares.
- This moves like a king in chess.
- Surrounding it on all squares there is a bubble of protection. This does the following:
	1. Prevents a Slasher from killing a piece.
	2. Grants a 50/50 saving roll for a Queen attack. If the queen fails it remains in its original spot.
	3. Grants the protector the same buffs as other pieces.
- The protector can attack like a Slasher, but it's chances of winning are 25% vs. 75% for losing.

### Destroyer
- You get one of these in the one remaining spot.
- This piece moves like a king.
- This is the only ranged piece. It can, as a move, initiate an attack on any piece in line-of-sight. The saving throw for dying from this attack is 50%/50%.


## The Rules
There are Gold and Silver pieces. The gold player moves first. Each player gets to move one piece a turn unless some special action of the piece allows a second move.

The goal of the game is to eliminate all of the other player's pieces. A stalemate can be declared if no viable way to eliminate all of the other player's pieces is possible.