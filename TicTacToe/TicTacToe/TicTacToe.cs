﻿namespace TicTacToe
{
    // The overarching class
    class TicTacToe
    {
        // The player class
        class Player
        {
            public Guid Id { get; set; }
            public bool isX { get; set; }
            public string Name { get; set; }

            public Player(Guid id, bool is_x, string playerame)
            {
                Id = id;
                isX = is_x;
                Name = playerame;
            }

            // Public constructor for when info not given
            public Player()
            {
                Id = Guid.NewGuid();
                Console.WriteLine("Are you X? 0 for yes.");
                int X = Convert.ToInt32(Console.ReadLine());
                if (X == 0)
                {
                    isX = true;
                }
                if (X != 0)
                {
                    isX = false;
                }
                Console.WriteLine("Please enter your name.");
                string name = Console.ReadLine();
                Name = name;
            }
        }
        // Making the players
        Player player1 = new Player();
        Player player2 = new Player();

        // Making the board
        bool[,] TheBoard = new bool[3, 3];

        class Position
        {
            public int X { get; set; }
            public int Y { get; set; }
            
        }


    }

}
